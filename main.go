package main

// External packages being used
import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"path"
	"strconv"
	"strings"
	"time"

	"google.golang.org/api/googleapi/transport"
	youtube "google.golang.org/api/youtube/v3"

	"github.com/Jeffail/gabs"
	"github.com/bwmarrin/discordgo"
)

// Variables used for command line params
var (
	Email    string
	Password string
)

type queryParams struct {
	query string
}

// Init function always runs before main function (initializes)
func init() {

	fmt.Println(developerKey)
	fmt.Println("Initializing bot Token..")
	flag.StringVar(&Email, "e", "", "Account Email")
	flag.StringVar(&Password, "p", "", "Account Password")
	flag.Parse()
}

var developerKey = "AIzaSyDtmrv4jtsUsEZgp89iPc-ECUWJ-eA-nUY"

func main() {
	// Create a new Discord session
	dg, err := discordgo.New(Email, Password)
	// If error exists
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	// Register messageCreate as a callback for the messageCreate events.
	dg.AddHandler(messageCreate)

	// Open the websocket and begin listening
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	fmt.Println("Ticky is now running. Press CTRL-C to exit.")

	// Simple way to keep program running until CTRL-C is pressed
	<-make(chan struct{})
	return
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the authenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Print message to stdout.
	fmt.Printf("%20s %20s %20s > %s\n", m.ChannelID,
		time.Now().Format(time.Stamp), m.Author.Username, m.Content)

	if strings.HasPrefix(m.Content, ".help") {
		data, err := ioutil.ReadFile("./text/help.txt")
		if err != nil {
			log.Fatal(err)
		}
		dataString := string(data[:])
		_, _ = s.ChannelMessageSend(m.ChannelID, dataString)
	}

	if strings.HasPrefix(m.Content, ".mp3") {
		mp3Search := m.Content[4:]
		message := searchVK(mp3Search)
		_, _ = s.ChannelMessageSend(m.ChannelID, message)
	}

	if strings.HasPrefix(m.Content, ".yt") {
		ytSearch := m.Content[4:]
		message := youtubeSearch(ytSearch)
		fmt.Println(message)
		if message != "" {
			_, _ = s.ChannelMessageSend(m.ChannelID, message)
		} else {
			_, _ = s.ChannelMessageSend(m.ChannelID, "**No videos where found!**")
		}
	}

	if strings.HasPrefix(m.Content, ".gi") {
		searchImg := m.Content[4:]
		message := imageSearch(searchImg)
		if message != "" {
			_, _ = s.ChannelMessageSend(m.ChannelID, message)
		} else {
			_, _ = s.ChannelMessageSend(m.ChannelID, "**No images where found!**")
		}

	}

	if strings.Contains(m.Content, ".ca") {
		imageURL := m.Attachments[0].URL
		if imageURL != "" {
			contentAware(imageURL)
			file, err := os.Open("./image.jpg")
			if err != nil {
				_, _ = s.ChannelMessageSend(m.ChannelID, "**Error Scanning file!**")
			}

			_, _ = s.ChannelFileSend(m.ChannelID, "image.jpg", file)
			go exec.Command("rm", "-rf", "image.jpg")

		} else {
			_, _ = s.ChannelMessageSend(m.ChannelID, "**No can do!**")

		}

	}
}

func contentAware(imageURL string) {
	extension := path.Ext(imageURL)
	fmt.Println(imageURL)
	downloadFile("./image"+extension, imageURL)

	cmd := "convert"
	args := []string{"image" + extension, "-liquid-rescale", "50%", "image.jpg"}

	args2 := []string{"image.jpg", "-resize", "600x600", "image.jpg"}
	exec.Command(cmd, args...).Run()
	exec.Command(cmd, args2...).Run()
}

func downloadFile(filepath string, url string) (err error) {

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}

func searchVK(query string) string {
	messageToReturn := ""
	if query != "" {
		url := fmt.Sprintf("https://api.datmusic.xyz/search?q={%s}", query)
		resp, err := http.Get(url)
		if err != nil {
			messageToReturn = "**Could not fetch from source!**"
			return messageToReturn
		}
		// We must close resp.Body on all execution paths.
		// (Chapter 5 presents 'defer', which makes this simpler.)
		if resp.StatusCode != http.StatusOK {
			resp.Body.Close()
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			messageToReturn = "**Could not fetch from source!**"
			return messageToReturn
			// panic(err.Error())
		}

		jsonParsed, _ := gabs.ParseJSON([]byte(body))

		title := jsonParsed.Path("data.title")
		link := jsonParsed.Path("data.stream")
		duration := jsonParsed.Path("data.duration")
		arraySize, _ := title.ArrayCount()

		result := ""
		if arraySize < 1 {
			messageToReturn = "**No results found!**"
			return messageToReturn
		}
		if arraySize >= 1 {
			for i := 0; i <= 4; i++ {
				Title, _ := title.ArrayElement(i)
				stringTitle := Title.String()

				Link, _ := link.ArrayElement(i)
				stringLink := Link.String()

				duration, _ := duration.ArrayElement(i)
				stringDuration := duration.String()
				intDuration, _ := strconv.Atoi(stringDuration)
				minDuration := secondsToMinutes(intDuration)

				if intDuration != 0 {
					result += "Found **"
					result += stringTitle[1 : len(stringTitle)-1]
					result += "** (" + minDuration + ("m)")
					result += " at "
					result += stringLink[1 : len(stringLink)-1]
					result += "\n"
				} else {
					return result
				}
			}
		}
		messageToReturn = result
		return messageToReturn
	}
	messageToReturn = "**Please enter a query!**"
	return messageToReturn
}

func secondsToMinutes(inSeconds int) string {
	minutes := inSeconds / 60
	seconds := inSeconds % 60
	str := fmt.Sprintf("%d:%d", minutes, seconds)
	return str
}

func youtubeSearch(input string) string {

	videoURL := ""

	var MaxResults int64
	MaxResults = 1
	client := &http.Client{
		Transport: &transport.APIKey{Key: developerKey},
	}

	service, err := youtube.New(client)
	if err != nil {
		videoURL = "**Error creating new YouTube client!**"
	}

	// Make the API call to YouTube.
	call := service.Search.List("id,snippet").
		Q(input).
		MaxResults(MaxResults)
	response, err := call.Do()
	if err != nil {
		videoURL = "**Error making search API call**"
	}

	// Group video, channel, and playlist results in separate lists.
	videos := make(map[string]string)
	channels := make(map[string]string)
	playlists := make(map[string]string)

	// Iterate through each item and add it to the correct list.
	for _, item := range response.Items {
		switch item.Id.Kind {
		case "youtube#video":
			videos[item.Id.VideoId] = item.Snippet.Title
		case "youtube#channel":
			channels[item.Id.ChannelId] = item.Snippet.Title
		case "youtube#playlist":
			playlists[item.Id.PlaylistId] = item.Snippet.Title
		}
	}

	for id := range videos {
		videoURL = "https://www.youtube.com/watch?v=" + id

	}
	if videoURL == "" {
		for id := range channels {
			videoURL = "https://www.youtube.com/channel/" + id
		}
	}

	if videoURL == "" {
		for id := range playlists {
			videoURL = "https://www.youtube.com/playlist?list=" + id
		}
	}
	if videoURL == "" {
		videoURL = "**No videos where found!**"
	}

	return videoURL

}

func imageSearch(query string) string {
	messageToReturn := ""
	correctedQuery := strings.Replace(query, " ", "%20", -1)
	toSearch := queryParams{correctedQuery}
	imageSearch := toSearch.createSearchURL()
	fmt.Println(imageSearch)

	resp, err := http.Get(imageSearch)
	if err != nil {
		messageToReturn = "**Could not fetch from source!**"
		return messageToReturn
	}
	// We must close resp.Body on all execution paths.
	// (Chapter 5 presents 'defer', which makes this simpler.)
	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		messageToReturn = "**Could not fetch from source!**"
		return messageToReturn
	}

	jsonParsed, _ := gabs.ParseJSON([]byte(body))
	value := jsonParsed.Path("items.link")
	size, _ := value.ArrayCount()
	var links = make([]string, size)

	for i := range links {
		title, _ := value.ArrayElement(i)
		stringTitle := title.String()

		links[i] = stringTitle[1 : len(stringTitle)-1]
	}

	return links[rand.Intn(len(links)-1)]

}

func (s queryParams) createSearchURL() string {

	searchURL := []string{"https://www.googleapis.com/customsearch/v1?"}
	query := make(map[string]string)
	query["q"] = s.query
	query["searchType"] = "image"
	query["fields"] = "items(link)"
	query["cx"] = "005994192710747203831:47rek7zx7ra"
	query["key"] = "AIzaSyDtmrv4jtsUsEZgp89iPc-ECUWJ-eA-nUY"

	for k, v := range query {
		searchURL = append(searchURL, k, "=", v, "&")
	}

	return strings.Join(searchURL, "")
}
